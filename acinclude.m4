dnl This file is part of GSC
dnl Copyright (C) 2007 Sergey Poznyakoff
dnl
dnl GSC is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl GSC is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with GSC.  If not, see <http://www.gnu.org/licenses/>.

dnl MU_RESULT_ACTIONS -- generate shell code for the result of a test
dnl   $1 -- CVAR  -- cache variable to check
dnl   $2 -- NAME  -- if not empty, used to generate a default value TRUE:
dnl                  `AC_DEFINE(HAVE_NAME)'
dnl   $2 -- TRUE  -- what to do if the CVAR is not `no'
dnl   $3 -- FALSE -- what to do otherwise; defaults to `:'
dnl
AC_DEFUN([MU_RESULT_ACTIONS], [
[if test "$$1" != "" -a "$$1" != no; then
  ]ifelse([$3], ,
          [AC_DEFINE(HAVE_]translit($2, [a-z ./<>], [A-Z___])[,1,[FIXME])],
          [$3])[
else
  ]ifelse([$4], , [:], [$4])[
fi]])dnl

dnl Arguments:
dnl   $1     --    Library to look for
dnl   $2     --    Function to check in the library
dnl   $3     --    Any additional libraries that might be needed
dnl   $4     --    Action to be taken when test succeeds
dnl   $5     --    Action to be taken when test fails
dnl   $6     --    Directories where the library may reside
AC_DEFUN([MU_CHECK_LIB],
[
  save_LIBS=$LIBS
  AC_CACHE_CHECK([for -l$1], mu_cv_lib_$1,
  [
   for path in $6
   do
      LIBS="$save_LIBS -L$path"
      unset ac_cv_lib_$1_$2
      AC_CHECK_LIB($1, $2,
                   [mu_cv_lib_$1="$3 -L$path -l$1"
                    break],
                   [mu_cv_lib_$1=no],$3)
   done
  ])
  MU_RESULT_ACTIONS([mu_cv_lib_$1],[LIB$1],[$4],[$5])
  LIBS=$save_LIBS
])
