#! /bin/sh
#  Copyright (C) 1999, 2005 Sergey Poznyakoff
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program. If not, see <http://www.gnu.org/licenses/>.

# Earliest RCS Id: consoleconf,v 1.2 1999/06/19 11:34:07 gray Exp gray $

PROGNAME=`basename $0`
LANGDIR=${CONSOLECONFDIR:-!CONSOLECONFDIR!}
MSGDIR=$LANGDIR/motd
FONTDIR=${CONSOLEFONTSDIR:-!CONSOLEFONTSDIR!}
SCRNMAPDIR=${CONSOLETRANSDIR:-!CONSOLETRANSDIR!}
DEFSCRNMAP=trivial
SYSTEMKEYMAP=/etc/keymap

usage() {
	echo "usage: $PROGNAME [-c charset] lang"
}

# usage: execute PROGNAME ARG 
execute() {
    eval $*
}

load_keys() {
    execute loadkeys $KEYMAP
    [ -r $SYSTEMKEYMAP ] && loadkeys $SYSTEMKEYMAP 
}

## Start
#

while getopts "hc:" OPTION
do
    case $OPTION in	
    h)	usage; exit 0;;
    c)	CHARSET="-"$OPTARG; shift 2;;
    *)	echo "Unknown option. Try $PROGNAME -h for more info" >&2; exit 1
    esac
done

shift $(($OPTIND - 1))	

if [ $# -ne 1 ]; then
    echo "Missing argument. Try $PROGNAME -h for more info" >&2
    exit 1
fi

LANG=$1

if [ -r ${LANGDIR}/${LANG}$CHARSET ] ; then
    . ${LANGDIR}/${LANG}$CHARSET
else
    if [ x"$CHARSET" = x"" ] ; then
	echo "$PROGNAME: Unknown language: $LANG"
    else
	echo "$PROGNAME: Unknown language-charset: ${LANG}$CHARSET"
    fi
    exit 1
fi

if [ -z "$DISPLAY" ]; then
    [ -n "$KEYMAP" ] && load_keys
    [ -n "$FONT" ]  && execute setfont $FONT
    if [ -n "$SCRNMAP" ] ; then 
	execute mapscrn $SCRNMAP
    else
	execute mapscrn $DEFSCRNMAP
    fi

    echo -ne "\033(K"              # the magic sequence

    if [ -n "$MOTD" ] ; then
	P=`expr "$MOTD" : '@\(.*\)'`
	if [ -z "$P" ] ; then
	    echo $MOTD
	else
 	    cat ${MSGDIR}/$P
	fi
    fi
else
    if [ -n "$X11" ]; then
	eval setxkbmap $X11
    else
	echo "`basename $0`: X11 is not set in ${LANGDIR}/${LANG}$CHARSET" >&2
	exit 1
    fi		
fi	

## End
