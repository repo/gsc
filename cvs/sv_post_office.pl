#! /usr/bin/perl
#
# Copyright (C) 2005, 2006, 2007 Sergey Poznyakoff <gray--at--gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Getopt::Long;
use Savane;

use POSIX qw(strftime);

# Import
our $sys_mail_domain;

my $script = "sv_post_office";
my $logfile = "/var/log/sv_database2system.log";

# Preconfigure
my $getopt;
my $help;
my $debug;
my $cron;
my $make_never;
my $make_always;

# Script-specific variables. These should probably be declared in
# savane.conf.pl
my $min_uid = 5000; # FIXME: These two are duplicated in several other sv_
my $min_gid = 5000; #        scripts. Time to move them to savane.conf.pl

my $post_office='post-office'; # Unix name of the 'post office' project.

my $listfile="/etc/mail/pop-users"; # Location of the alias file.

my $tmpfile="/tmp/list.$$.tmp"; # New alias file is created here. 

eval {
    $getopt = GetOptions("debug" => \$debug,
			 "post-office=s" => \$post_office,
			 "file=s" => \$listfile,
			 "no-make" => \$make_never,
			 "make-never" => \$make_never,
			 "make-always" => \$make_always,
                         "cron" => \$cron,
                         "help" => \$help);
};

if($help || !$getopt) {
    print STDERR <<EOF;
Usage: $0 [OPTIONS]
Create mail alias file for Savane users not having pop accounts.

OPTIONS are:

      --cron                   Option to set when including this script
                               in a crontab
      --debug                  Do nothing, print everything
      --file=FILE              Output aliases to FILE
                               (default $listfile)
      --no-make, --make-never  Do not run make after processing
      --make-always            Always run make, even if nothing changed
      --post-office=NAME       Name of the post office project
                               (default $post_office)

      --help                   Show this help and exit

Author: gray\@gnu.org 
EOF
 exit(1);
}

# Log: Starting logging
open (LOG, ">>$logfile");
print LOG strftime "[$script] %c - starting\n", localtime;

# Locks: There are several sv_db2sys scripts but they should not run
#        concurrently.  So we add a lock
AcquireReplicationLock();

my %pop_users;

foreach my $line (GetDB("user,user_group,groups",
                        "user_group.user_id=user.user_id AND user_group.group_id=groups.group_id AND groups.unix_group_name='$post_office'",
			"user.user_name,user.email")) {
    chomp($line);
    my ($name, $email) = split(",", $line);

    my ($name,$passwd,$uid,$gid,
        $quota,$comment,$gcos,$dir,$shell,$expire) = getpwnam($name);
    next if ($uid < $min_uid) or ($gid < $min_gid);
    print "DEBUG: POP user: $name\n" if $debug;
    
    $pop_users{$name}=1;
}

unless ($debug) {
    open(TMP, "> $tmpfile") or do {
	print LOG strftime "[$script] %c - Failed to open $tmpfile for writing.\n", localtime;
	exit(1);
    };
    print TMP "# Aliases for users not having POP access to this box\n"
}

foreach my $line (GetDB("user", 
			"status='A'",
			"user_name,email")) {
    chomp($line);
    my ($name, $email) = split(",", $line);
#    print "DEBUG: LINE $line\n" if $debug;

    next if $pop_users{$name};
    my ($name,$passwd,$uid,$gid,
        $quota,$comment,$gcos,$dir,$shell,$expire) = getpwnam($name);

    next if ($uid < $min_uid)
	 or ($gid < $min_gid)
	 or -f "/home/$name/.forward"
	 or $email =~ /\@$sys_mail_domain/o;
    
    print LOG "[$script] ALIAS: $name: $email\n";
    if ($debug) {
	print "DEBUG: ALIAS: $name: $email\n";
    } else {
	print TMP "$name: $email\n";
    }
}

my $retcode;

unless($debug) {
    close(TMP); 
	
    system("cmp $listfile $tmpfile >/dev/null 2>&1");
    if ($? != 0 or $make_always) {
	if ($make_never) {
	    my $output = `cp $tmpfile $listfile 2>&1`;
	    my $res = $?;
	    foreach my $line (split("\n", $output)) {
		print LOG strftime "[$script] $line\n", localtime;
	    }
	    if ($res != 0) {
		print LOG strftime "[$script] %c - cp failed with code $?.\n", localtime;
	    }
	    print LOG strftime "[$script] NOT running make (--no-make given).\n",
	          localtime;
	    $retcode = 0	     
	} else {
	    my $output;
	    if ($make_always) {
		$output = `make -C /etc/mail aliases.db 2>&1`;
	    } else {
		$output = `cp $tmpfile $listfile 2>&1 && make -C /etc/mail aliases.db 2>&1`;
	    }
	    my $res = $?;
	    foreach my $line (split("\n", $output)) {
		print LOG strftime "[$script] $line\n", localtime;
	    }
	    if ($res != 0) {
		print LOG strftime "[$script] %c - make failed with code $?.\n", localtime;
	    }
	    $retcode = $res
	}
    } else {
	$retcode = 1
    }
    unlink($tmpfile);
}

# Final exit
print LOG strftime "[$script] %c - work finished\n", localtime;
print LOG "[$script] ------------------------------------------------------\n";

exit $retcode;
# END
