;;;; General settings

(put 'eval-expression 'disabled nil)
(setq enable-local-eval t)

(global-set-key "\M-s" 'goto-line)
(setq inferior-lisp-program "/usr/local/bin/guile --debug")

;;; Some settigns depend on window system
(cond
 ((not window-system)
  (menu-bar-mode -1)               ; I hate menu bars.
  (standard-display-european t))
 (t
  (set-language-environment 'utf-8)
  (global-font-lock-mode 1)

  ;; Set up Arabic fonts when using emacs-bidi
  (when (boundp 'emacs-bidi-version)
    (set-fontset-font
     "fontset-default"
     (cons (decode-char 'ucs #x05b0) (decode-char 'ucs #x06ff))
     "-m17n-*--20-*-iso10646-1")
    (set-fontset-font
     "fontset-default"
     (cons (decode-char 'ucs #xfb2a) (decode-char 'ucs #xfbff))
     "-m17n-*--20-*-iso10646-1")
    (set-fontset-font
     "fontset-default"
     (cons (decode-char 'ucs #xfe70) (decode-char 'ucs #xfefc))
     "-m17n-*--20-*-iso10646-1")
    (set-fontset-font
     "fontset-default"
     (cons (decode-char 'ucs #x200c) (decode-char 'ucs #x200f))
     "-m17n-*--20-*-iso10646-1"))  ))

;; When using emacs-bidi, provide paths to my usual site directories
(when (boundp 'emacs-bidi-version)
 (setq load-path (append load-path 
                         (list "/usr/share/emacs/site-lisp" 
                               "/usr/local/share/emacs/site-lisp"))))
;; More paths
(setq load-path (append 
                 '(;"/home/gray/mh-e/src"
                   "/home/gray/.elisp" 
                   "/usr/local/mailutils/share/emacs/site-lisp")
                 load-path))

;; Load necessary packages

(autoload 'sc-cite-original     "supercite")
(autoload 'sc-submit-bug-report "supercite")

(setq user-mail-address "gray@gnu.org.ua")

; Configure MH
;(load "mailutils-mh")
(require 'mh-e)
(setq mh-compose-insertion 'mhn)
(setq mh-invisible-header-fields (list "X-Mailutils-Message-Id:"))

(require 'erc)
(setq erc-anonymous-login nil
      erc-nick "sergiusz"
      erc-server "irc.gnu.org"
      erc-port "ircd")

(require 'mailcrypt)
(mc-setversion "gpg")
(add-hook 'mh-folder-mode-hook 'mc-install-read-mode)
(add-hook 'mh-letter-mode-hook 'mc-install-write-mode)
(setq mc-pgp-user-id "0768366A") ;; RIPE key ID is the primary one
(setq mc-passwd-timeout 120)

;; Specific setups:
(load "~/lib/emacs.el")
(load "~/lib/email-settings.el")
(load "~/lib/spell-check.el")
(require 'po-mode)
(load "~/lib/po.el")
(add-hook 'po-mode-hook 'gray-po-mode-hook)
(add-hook 'po-subedit-mode-hook 'gray-po-subedit-mode-hook)
(add-hook 'po-subedit-exit-hook 'gray-po-subedit-exit-hook)

(load "~/lib/links-mode.el")

;; Load directory-specific settings:

(let ((local-rc "./.emacsrc"))
  (if (file-exists-p local-rc)
      (load-file local-rc)))

;; Restore desktop
(load "desktop")
(desktop-load-default)
(desktop-read)

;;;; End of .emacs
