;;;; emacs.el -- Provide mail modification hooks
;; Copyright (C) 2001, 2005, 2007 Sergey Poznyakoff

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defvar gray-to-alist nil
  "Contains a list of SMTP header strings to be appended to the standard
header when sending mail to specific addresses

Each entry is of the form:

	(DEST-ADDR HEADER VALUE)")

;; Produce a list of headers to be added to the message
;; Each list item is a cons of the form:
;;	(HEADER . VALUE)
(defun gray-header-to (headers dest)
  (let ((addr (if (string-match "<\\(.*\\)>" dest)
		  (substring dest (match-beginning 1) (match-end 1))
		dest)))
    (mapcan (function (lambda (x)
			(when (or
			       (and (functionp (car x))
				    ((car x) addr))
			       (and (stringp (car x))
				    (string-equal addr (car x))))
			  (cdr x))))
	    gray-to-alist)))

(defun gray-header-subject (headers dest)
  (if (string-match "\\s confirm [0-9a-z]+$" dest)
      (let ((elt (assoc "Fcc" headers)))
	(setcdr elt nil))))

;; Modify message headers according to destination address
;; NOTE: "Fcc: sent-messages" gets appended anyway.
(defun gray-mh-send-hook ()
  (save-excursion
    (goto-char (point-min))
    (let ((add-header (list (cons "Fcc" "sent-messages"))))
      (while (and (not (eobp))
		  (not (or (looking-at "^\\s *$")
			   (looking-at "^-+\\s *$")))
		  (looking-at "\\([^:]*\\): *\\(.*\\)"))
	(let* ((kw (buffer-substring (match-beginning 1)
				     (match-end 1)))
	       (arg (buffer-substring (match-beginning 2)
				      (match-end 2)))
	       (fun (intern (concat "gray-header-" (downcase kw))))
	       (hdr (and (fboundp fun)
			 (functionp fun)
			 (funcall fun add-header arg))))
	  (cond
	   ((not hdr))
	   ((consp hdr)
	    (setq add-header (append add-header (list hdr))))
	   ((listp hdr)
	    (setq add-header (append add-header hdr)))))

	(forward-line 1)
	(while (looking-at "[ \t]") (forward-line 1)))
      (mapcar (function (lambda (x)
			  (if (and (not (null x))
				   (cdr x))
			      (insert (concat
				       (car x) ": " 
				       (if (listp (cdr x))
					   (cadr x)
					 (cdr x))
				       "\n")))))
	      add-header))))

(add-hook 'mh-before-send-letter-hook 'gray-mh-send-hook)

