;; ---------------------------------------------------------------------------
;; Prepare C indentation style &c.
(load "cc-mode")
(c-add-style "bsd-8" '((c-basic-offset . 8)))

(setq c-mode-common-hook (function 
                            (lambda () 
			      (c-set-style
                                (if (boundp 'c-global-style)
					c-global-style
				 "bsd-8")))))
;(setq c-mode-common-hook (function (lambda () (c-set-style "bsd-8"))))
;; 
;; -------------------------------------------------------------------------
;; Set default minor modes
(line-number-mode t)

;; -------------------------------------------------------------------------
;; autoload
(autoload 'po-mode "po-mode")

;; -------------------------------------------------------------------------
;; vi-like parenthesis-matching shortcut
(defun match-paren (arg)
 (interactive "p")
 (cond
  ((looking-at "\\s\(")
   (forward-list 1)
   (backward-char 1))
  ((looking-at "\\s\)")
   (forward-char 1)
   (backward-list 1))
  (t
   (self-insert-command (or arg 1)))))

(global-set-key "%" 'match-paren)

;; -------------------------------------------------------------------------
;; Enable/disable used/unused commands
(put 'set-fill-column 'disabled t)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'eval-expression 'disabled nil)

;; -------------------------------------------------------------------------
;; Fine-tuning
(setq text-mode-hook 'turn-on-auto-fill)
(setq make-backup-files t)
(setq visible-bell t) 
(set-input-mode (car (current-input-mode))
		(nth 1 (current-input-mode))
		0)
(setq auto-mode-alist (mapcar 'purecopy
                              '(("\\.c$" . c-mode)
                                ("\\.h$" . c-mode)
				("\\.y$" . c-mode)
				("\\.l$" . c-mode)
				("\\.lex$" . c-mode)
				("\\.cc$" . c++-mode)
				("\\.C$" . c++-mode)
				("\\.cxx$" . c++-mode)
				("\\.cpp$" . c++-mode)
                                ("\\.s$" . asm-mode)
				("\\.S$" . asm-mode)
				("\\.awk$" . awk-mode)
				("\\.pl$" . perl-mode) 
				("[Mm]akefile" . makefile-mode)
                                ("\\.tex$" . TeX-mode)
                                ("\\.txi$" . texinfo-mode)
                                ("\\.texi$" . texinfo-mode)
                                ("\\.texinfo$" . texinfo-mode)
                                ("\\.el$" . emacs-lisp-mode)
                                ("\\.lisp$" . lisp-mode)
				("\\.scm$" . scheme-mode)
                                ("\\.a$" . c-mode)
				("\\.tar$" . tar-mode)
				("\\.P$" . prolog-mode)
				("\\.po[tx]?\\'\\|\\.po\\." . po-mode)
				("\\.tcl$" . tcl-mode)
				("\\.exp$" . tcl-mode)
;;				("\\.st$" . smalltalk-mode)
;;				("\\.htm$" . html-mode)
;;				("\\.html$" . html-mode)
;;				("\\.[cg]fl$" . gflow-mode)
;;				("\\.[cg]flow$" . gflow-mode)
				("[]>:/]\\..*emacs.*\\'" . emacs-lisp-mode))))

(setq inferior-lisp-program "/usr/local/bin/gcl")

;; -------------------------------------------------------------------------
;; Reset some keybindings 
(global-set-key [insertchar] 'overwrite-mode)
(global-set-key "\015" 'newline-and-indent)
(global-set-key "\011" 'tab-to-tab-stop)
(global-set-key [kp-7] 'beginning-of-line)
(global-set-key [kp-1] 'end-of-line)
(global-set-key [end] 'end-of-line)
(global-set-key "\033[U" 'scroll-up)
(global-set-key "\033[V" 'scroll-down)
;;(global-set-key [f5] 'isearch-toggle-regexp)
(global-set-key [f6] 'search-forward-regexp)
(global-set-key [f7] 'search-backward-regexp)
(global-set-key [f8] 'expand-abbrev)
(global-set-key [f9] 'compile)
(global-set-key [f10] 'gdb)
;;(global-set-key [f11] 'goto-line)
;;(global-set-key [f12] 'find-tag-regexp)
;;lobal-set-key [f13] 'isearch-toggle-regexp)

(global-set-key [?\C-x deletechar] 'delete-region)

;; -------------------------------------------------------------------------
;; Autoloads
(setq load-path (cons (expand-file-name "PATH_FOR_AUC-TEX") load-path))
(autoload 'tex-mode "auc-tex" "Automatic select TeX or LaTeX mode" t)
(autoload 'plain-tex-mode "auc-tex" "Mode for Plain TeX" t)
(autoload 'latex-mode "auc-tex" "Mode for LaTeX" t)
(autoload 'LaTeX-math-mode    "tex-math"      "Math mode for TeX." t)
(autoload 'outline-minor-mode "outline-m" "Minor Outline Mode." t)

;; -------------------------------------------------------------------------
;; Keyboard translation (when needed)
;(keyboard-translate ?\C-h ?\C-?)
;(keyboard-translate ?\C-? ?\C-h)

;;; Emacs/W3 Configuration
(setq load-path (cons "/usr/local/share/emacs/site-lisp" load-path))
(condition-case () (require 'w3-auto "w3-auto") (error nil))

;;;
;(setq mh-progs "/usr/bin/mh")
;(setq mh-lib "/usr/lib/mh")
(setq load-path (cons "/usr/local/mailutils/share/emacs/site-lisp" load-path))
(load "mailutils-mh")
(setq mh-compose-insertion 'mhn)

