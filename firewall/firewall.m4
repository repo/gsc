#! /bin/sh
dnl -*- m4 -*-
dnl Copyright 2005, 2006 Sergey Poznyakoff
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2, or (at your option)
dnl any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
dnl MA 02110-1301, USA.

changequote([,])
divert(-1)
ifdef([IPTABLES],[],[define([IPTABLES],[/usr/sbin/iptables])])
define([any],[0.0.0.0/0])

dnl       $1     $2    $3     $4      $5     $6    $7      
dnl __rule__(chain,target,srcip,srcport,dstip,dstport,proto)
define([__rule__],[dnl
IPTABLES -A $1 -j [$2] dnl
ifelse([$7],,[ifelse([$4][$6],,,[--protocol all])],[--protocol $7]) dnl
ifelse([$3],,,[--source $3 ]ifelse([$4],,,[--source-port $4 ]))dnl
ifelse([$5],,,[--destination $5 ]ifelse([$6],,,[--destination-port $6]))])

dnl         $1     $2    $3     $4      $5     $6   $7      
dnl ACCEPT(chain,srcip,srcport,dstip,dstport,proto,log)
define([ACCEPT],[ifelse($7,,,__rule__($1,[LOG],$2,$3,$4,$5,$6)
)]dnl
[__rule__($1,[ACCEPT],$2,$3,$4,$5,$6)])

dnl         $1     $2    $3     $4      $5     $6   $7      
dnl DENY(chain,srcip,srcport,dstip,dstport,proto,log)
define([DENY],[ifelse($7,,,__rule__($1,[LOG],$2,$3,$4,$5,$6)
)]dnl
[__rule__($1,[DROP],$2,$3,$4,$5,$6)])

dnl         $1     $2    $3     $4      $5     $6   $7      
dnl REJECT(chain,srcip,srcport,dstip,dstport,proto,log)
define([REJECT],[ifelse($7,,,__rule__($1,[LOG],$2,$3,$4,$5,$6)
)]dnl
[__rule__($1,[REJECT],$2,$3,$4,$5,$6)])

define([LIST],[IPTABLES --list])
ifdef([SYSCONFDIR],,[define([SYSCONFDIR],[/etc/firewall])])
divert(0)

# port_setup iface dest
port_setup() {
  (dest=$2
   cd SYSCONFDIR/rule.d/$1
   for name in `ls * 2>/dev/null`
   do
     case $name in
     [[0-9]]*:udp) 
       port=`expr $name : '\(.*\):udp'`
       while read ip REST
       do
         case $ip in
         \#.*) ;;
         '')   ;;
         \$*)  eval ip=$ip
               ACCEPT(INPUT, $ip,, $dest,$port, udp)
               ;;
         *)    ACCEPT(INPUT, $ip,, $dest,$port, udp)
               ;;
         esac
       done < SYSCONFDIR/rule.d/$1/$name
       DENY(INPUT, any,, $dest,$port, udp, LOG)
       ;;
     [[0-9]]*:tcp)
       port=`expr $name : '\(.*\):tcp'`
       while read ip
       do
         case $ip in
         \#.*) ;;
         '')   ;;
         \$*)  eval ip=$ip
               ACCEPT(INPUT, $ip,, $dest,$port, tcp)
               ;;
         *)    ACCEPT(INPUT, $ip,, $dest,$port, tcp)
               ;;
         esac
       done < SYSCONFDIR/rule.d/$1/$name
       DENY(INPUT, any,, $dest,$port, tcp, LOG)
       ;;
     esac
   done)
}


ifelse(ACTION,status,dnl
dnl
LIST,
dnl
ACTION,flush,dnl
[IPTABLES -F FORWARD
IPTABLES -F INPUT
IPTABLES -F OUTPUT
IPTABLES -P FORWARD [ACCEPT]
IPTABLES -P INPUT [ACCEPT]
IPTABLES -P OUTPUT [ACCEPT]],dnl
[include(ACTION)])


dnl END

