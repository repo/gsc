dnl -*- m4 -*-
dnl Copyright 2005 Sergey Poznyakoff
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License along
dnl with this program. If not, see <http://www.gnu.org/licenses/>.

define([my_ip],[10.10.10.1])
define([eth1_ip],[192.168.1.1])
define([eth2_ip],[192.168.10.1])

######

port_setup [eth0] my_ip
port_setup [eth1] eth1_ip
port_setup [eth2] eth2_ip

# FIN
