/* This file is part of GSC
   Copyright (C) 2007 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <sys/types.h>
#include <stdio.h>
#include <time.h>

#if defined HAVE_SYSCONF && defined _SC_OPEN_MAX
# define getmaxfd() sysconf(_SC_OPEN_MAX)
#elif defined (HAVE_GETDTABLESIZE)
# define getmaxfd() getdtablesize()
#else
# define getmaxfd() 256
#endif

void gsc_version (const char *progname);

int gsc_userprivs (uid_t uid, gid_t *grplist, size_t ngrp);
char *gsc_userprivs_errstring (void);

int gsc_str_to_syslog_facility (char *str, int *pfacility);
int gsc_str_to_syslog_priority (char *str, int *pprio);
const char *gsc_syslog_facility_to_str (int n);
const char *gsc_syslog_priority_to_str (int n);

int parse_time_interval (const char *str, time_t *pint, const char **endp);

extern int argcv_get    (const char *command, const char *delim,
			 const char* cmnt,
			 int *argc, char ***argv);
extern int argcv_get_n (const char *command, int len,
		        const char *delim, const char *cmnt,
			int *argc, char ***argv);
  
extern int argcv_string (int argc, char **argv, char **string);
extern int argcv_free   (int argc, char **argv);
extern int argcv_unquote_char (int c);
extern int argcv_quote_char   (int c);
extern size_t argcv_quoted_length (const char *str, int *quote);
extern void argcv_unquote_copy (char *dst, const char *src, size_t n);
extern void argcv_quote_copy (char *dst, const char *src);


typedef struct cfg_file
{
  FILE *fp;
  const char *file_name;
  unsigned line;
  dev_t dev;
  ino_t ino;
  struct cfg_file *prev;
  char *buf;
  size_t size;
  void (*error_msg) (const char *name, unsigned line, const char *fmt, ...)
       GSC_PRINTFLIKE(3,4);
  unsigned error_count;

  struct gsc_config_keyword *kwtab;
}
gsc_config_file_t;

typedef void (*gsc_config_fun) (gsc_config_file_t *file, char *kw,
				char *val, void *data);

struct gsc_config_keyword
{
  char *kw;
  gsc_config_fun handler;
};

typedef enum
  {
    gsc_config_success,
    gsc_config_error,
    gsc_config_open
  }
gsc_config_status_t;

void gsc_config_parse_block (gsc_config_file_t *file, void *data,
			     struct gsc_config_keyword *kwtab,
			     const char *end);
gsc_config_status_t gsc_config_parse (const char *config_file,
				      void (*errmsg) (const char *, unsigned,
						      const char *, ...),
				      struct gsc_config_keyword *kwh);

void gsc_config_include_handler (gsc_config_file_t *file, char *kw, char *val,
				 void *data);
int gsc_config_include_start (gsc_config_file_t *new_file,
			      gsc_config_file_t *file, char *name);
gsc_config_file_t *gsc_config_include_stop (gsc_config_file_t *pfile);

extern char *gsc_config_include_dir;
