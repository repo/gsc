#! /bin/sh
# svn-backup: SVN-based backup
# Copyright (C) 2006, 2007 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

C_OPTS=""
X_OPTS=""
DRY_RUN=
VERBOSE=0
MODULE=`hostname|tr A-Z a-z`
URL=svn+ssh://svn@svn.farlep.net/svn/machines
DIR=
ROOT=/
MESSAGE="Automatic commit by $0"

FROMFILE=/tmp/svn-backup.$$
cleanup() {
	rm -f $FROMFILE
}

trap 'cleanup' 1 2 13 15

error() {
    echo "$0: $*" >&2
}

verbose() {
    if [ $1 -le $VERBOSE ]; then
	shift
	echo "# $*"
    fi
}

usage() {
    cat <<-EOF
	usage: $0 [OPTIONS] [FILE...]
	OPTIONS are
	-d DIR        set working directory (default $DIR)
	-f FILE       read file names from FILE
	-h            display this help message
	-m STRING     set commit message
	-M MODULE     set module name (default $MODULE)
	-n            dry run
	-R DIR        root directory (default $ROOT)
	-u URL        set Subversion URL for initial commit
	              (default $URL)
	-v            increase verbosity
	-x PAT	      exclude pattern PAT
	-X FILE       read exclude patterns from FILE
	EOF
}

update() {
    verbose 1 "Executing tar $C_OPTS | tar $X_OPTS"
    if [ -z "$DRY_RUN" ]; then
        tar $C_OPTS | tar $X_OPTS
    else
        tar $C_OPTS | sed 's/^/# Copying /'
    fi
}

while getopts "d:hf:m:M:nR:x:X:u:v" OPTION
do
    case $OPTION in
    d) DIR=$OPTARG;;
    x) C_OPTS="$C_OPTS --exclude=$OPTARG";;
    X) C_OPTS="$C_OPTS -X $OPTARG";;
    f) C_OPTS="$C_OPTS -T $OPTARG";;
    h) usage; exit 0;;
    m) MESSAGE=$OPTARG;;
    M) MODULE=$OPTARG;;
    n) DRY_RUN=yes; VERBOSE=100;;
    u) URL=$OPTARG;;
    v) VERBOSE=$(($VERBOSE + 1));;
    R) ROOT=$OPTARG;;
    *) error "unknown option $OPTION";;
    esac
done

shift $(($OPTIND - 1))

C_OPTS="-C$ROOT $C_OPTS"
if [ $# -gt 0 ]; then
    C_OPTS="$C_OPTS $*"
fi

if [ -z "$DIR" ]; then
    DIR=/var/spool/backup/$MODULE
fi

if [ -e $DIR ]; then
    if [ ! -d $DIR ]; then
        error "$DIR exists but is not a directory"
	exit 1
    fi
    if [ ! -d $DIR/.svn ]; then
	error "$DIR is not controlled by SVN"
	exit 1
    fi
    if [ ! -d $DIR/trunk ]; then
	error "$DIR/trunk does not exist"
	exit 1
    fi
    (cd $DIR/trunk ; find . -name .svn -prune -o -type d -o -print) | 
      sed 's|^\./||' > $FROMFILE
    C_OPTS="$C_OPTS -T $FROMFILE"
    X_OPTS="-C$DIR/trunk -x -f - $X_OPTS"
else
    X_OPTS="-C$DIR -x -f - $X_OPTS"
fi

if [ $VERBOSE -gt 1 ]; then
    C_OPTS="$C_OPTS -v"
fi
if [ -n "$DRY_RUN" ]; then
    C_OPTS="-c -f /dev/null $C_OPTS"
    RUN=echo
else
    C_OPTS="-c -f - $C_OPTS"
    RUN=
fi

if [ -e $DIR ]; then
    update

    (cd $DIR;
     verbose 1 "Computing differences"
     if [ -z "$DRY_RUN" ]; then
         F=`svn status | grep '^\?' | while read x name; do echo $name; done`
	 if [ -n "$F" ]; then
	     verbose 1 "Adding new files: $F"
	     svn add $F
	 fi
     fi
     verbose 1 "Committing changes"
     $RUN svn commit -m "$MESSAGE" --non-interactive) 
else
    verbose 1 "Creating directory $DIR"
    $RUN mkdir $DIR || exit 1
    update
    $RUN svn mkdir -m 'Initial create' --non-interactive $URL/$MODULE || exit 1
    $RUN svn mkdir -m 'Initial create' --non-interactive $URL/$MODULE/trunk || exit 1
    $RUN svn mkdir -m 'Initial create' --non-interactive $URL/$MODULE/tags || exit 1
    $RUN svn mkdir -m 'Initial create' --non-interactive $URL/$MODULE/branches || exit 1
    ($RUN cd $DIR
     verbose 1 "Initial import"
     $RUN svn import -m 'Initial import' --non-interactive $URL/$MODULE/trunk)
    verbose 1 "Removing $DIR"
    $RUN rm -rf $DIR || exit 1
    verbose 1 "Initial checkout"
    $RUN svn checkout --non-interactive $URL/$MODULE
fi

cleanup
